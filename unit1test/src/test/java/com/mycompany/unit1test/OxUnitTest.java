/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.mycompany.unit1test;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author informatics
 */
public class OxUnitTest {
    
    public OxUnitTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testCheckWinRow1_O_output_true(){
        String[][] table = {{"O","O","O"},{"-","-","-"},{"-","-","-"}};
        String ceurrentPlayer = "O";
        boolean result = OX.checkWin(table,ceurrentPlayer);
        assertEquals(true,result);
    }@Test
    public void testCheckWinRow2_O_output_true(){
        String[][] table = {{"-","-","-"},{"O","O","O"},{"-","-","-"}};
        String ceurrentPlayer = "O";
        boolean result = OX.checkWin(table,ceurrentPlayer);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWinRow3_O_output_true(){
        String[][] table = {{"-","-","-"},{"-","-","-"},{"O","O","O"}};
        String ceurrentPlayer = "O";
        boolean result = OX.checkWin(table,ceurrentPlayer);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWinRow1_O_output_false(){
        String[][] table = {{"O","O","-"},{"-","-","-"},{"-","-","-"}};
        String ceurrentPlayer = "O";
        boolean result = OX.checkWin(table,ceurrentPlayer);
        assertEquals(false,result);
    }@Test
    public void testCheckWinRow2_O_output_false(){
        String[][] table = {{"-","-","-"},{"O","O","-"},{"-","-","-"}};
        String ceurrentPlayer = "O";
        boolean result = OX.checkWin(table,ceurrentPlayer);
        assertEquals(false,result);
    }
    @Test
    public void testCheckWinRow3_O_output_false(){
        String[][] table = {{"-","-","-"},{"-","-","-"},{"O","O","-"}};
        String ceurrentPlayer = "O";
        boolean result = OX.checkWin(table,ceurrentPlayer);
        assertEquals(false,result);
    }
    @Test
    public void testCheckWinCol1_O_output_true(){
        String[][] table = {{"O","-","-"},{"O","-","-"},{"O","-","-"}};
        String ceurrentPlayer = "O";
        boolean result = OX.checkWin(table,ceurrentPlayer);
        assertEquals(true,result);
    }@Test
    public void testCheckWinCol2_O_output_true(){
        String[][] table = {{"-","O","-"},{"-","O","-"},{"-","O","-"}};
        String ceurrentPlayer = "O";
        boolean result = OX.checkWin(table,ceurrentPlayer);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWinCol3_O_output_true(){
        String[][] table = {{"-","-","O"},{"-","-","O"},{"-","-","O"}};
        String ceurrentPlayer = "O";
        boolean result = OX.checkWin(table,ceurrentPlayer);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWinCol1_O_output_false(){
        String[][] table = {{"O","-","-"},{"O","-","-"},{"-","-","-"}};
        String ceurrentPlayer = "O";
        boolean result = OX.checkWin(table,ceurrentPlayer);
        assertEquals(false,result);
    }@Test
    public void testCheckWinCol2_O_output_false(){
        String[][] table = {{"-","O","-"},{"-","O","-"},{"-","-","-"}};
        String ceurrentPlayer = "O";
        boolean result = OX.checkWin(table,ceurrentPlayer);
        assertEquals(false,result);
    }
    @Test
    public void testCheckWinCol3_O_output_false(){
        String[][] table = {{"-","-","O"},{"-","-","O"},{"-","-","-"}};
        String ceurrentPlayer = "O";
        boolean result = OX.checkWin(table,ceurrentPlayer);
        assertEquals(false,result);
    }
    @Test
    public void testCheckWinX1_O_output_true(){
        String[][] table = {{"O","-","-"},{"-","O","-"},{"-","-","O"}};
        String ceurrentPlayer = "O";
        boolean result = OX.checkWin(table,ceurrentPlayer);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWinX2_O_output_true(){
        String[][] table = {{"O","-","O"},{"-","O","-"},{"O","-","-"}};
        String ceurrentPlayer = "O";
        boolean result = OX.checkWin(table,ceurrentPlayer);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWinX1_O_output_false(){
        String[][] table = {{"O","-","-"},{"-","-","-"},{"-","-","O"}};
        String ceurrentPlayer = "O";
        boolean result = OX.checkWin(table,ceurrentPlayer);
        assertEquals(false,result);
    }
    @Test
    public void testCheckWinX2_O_output_false(){
        String[][] table = {{"O","-","O"},{"-","-","-"},{"O","-","-"}};
        String ceurrentPlayer = "O";
        boolean result = OX.checkWin(table,ceurrentPlayer);
        assertEquals(false,result);
    }
    @Test
    public void testCheckDrawn_output_true(){
        String[][] table = {{"O","X","O"},{"O","X","O"},{"X","O","X"}};
        boolean result = OX.checkFullboard(table);
        assertEquals(true,result);
    }
    @Test
    public void testCheckDrawn_output_false(){
        String[][] table = {{"-","X","O"},{"O","X","O"},{"X","O","X"}};
        boolean result = OX.checkFullboard(table);
        assertEquals(false,result);
    }
    
   
    

 
  
    






}   
