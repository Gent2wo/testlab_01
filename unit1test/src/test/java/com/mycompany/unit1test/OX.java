/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.unit1test;

/**
 *
 * @author informatics
 */
class OX {

    static boolean checkWin(String[][] table, String currentPlayer) {      
        for (int r = 0; r < 3; r++) {
            if (table[r][0].equals(currentPlayer) && table[r][1].equals(currentPlayer) && table[r][2].equals(currentPlayer)) {
                return true;
            }
        }
        
        // CheckCol
        for (int c = 0; c < 3; c++) {
            if (table[0][c].equals(currentPlayer) && table[1][c].equals(currentPlayer) && table[2][c].equals(currentPlayer)) {
                return true;
            }
        }
        
        // CheckXY
        if ((table[0][0].equals(currentPlayer) && table[1][1] .equals(currentPlayer) && table[2][2] .equals(currentPlayer)) || (table[0][2].equals(currentPlayer) && table[1][1].equals(currentPlayer) && table[2][0].equals(currentPlayer))) {
            return true;
        }
        
        return false;
    }
    static boolean checkFullboard(String[][] table){
        for (int r = 0; r < 3; r++) {
            for (int c = 0; c < 3; c++) {
                if (table[r][c].equals("-")) {
                    return false;
                }
            }
        }
        return true;
    }
    

    
}
    
    
